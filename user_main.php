<?php
session_start();

require_once "models/User.php";
require_once "models/Credentials.php";
require_once "models/Transaction.php";

if(!Credentials::isLoggedIn()){
     header("Location: main.php");
     exit();
}
$currentUser = new User(unserialize($_SESSION['user'])->getIban());

if(isset($_POST['logout'])){
     Credentials::logOut();
     header("Location: main.php");
}
?>


<!doctype html>
<html lang="en">
<head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <link rel="stylesheet" href="css/bootstrap.min.css">
     <title>E-Banking</title>
     <script type="text/javascript" src="js/index.js"></script>
</head>
<body style="background-image: linear-gradient(to top, rgba(0,0,0,0), rgba(0,0,0,0.3)); background-repeat: no-repeat;">

<form action="user_main.php" method="post">


          <div class="row" align="center">
               <div class="col-sm-12">
                    <div class="container" style="width: 700px; height: 240px; background: white">
                         <h1><?=$currentUser->getFirstname() . " " . $currentUser->getSurname()?></h1>
                              <div>IBAN: <?= $currentUser->getIban()?></div><br>
                              <div>Kontoinhaber: <?= $currentUser->getFirstname() . " " . $currentUser->getSurname()?></div><br>
                              <div>Guthaben: <?=$currentUser->getBalance()?> €</div><br>
                              <div>BIC: SWPP787</div><br>
                    </div>
               </div>
          </div>
          <div class="row">
               <div class="container" style="width: 700px; height: 140px; background: lightgray; outline: black">
               <div class="col-sm-12" align="center">
                         <div style="margin-top: 5px">
                              <a href="transaction.php">Überweisen</a>
                         </div><br>
                         <div>
                              <A href="transactionDisplay.php">Transaktionen</A>
                         </div><br>

                    <div>
                         <input class="btn logoutButton" type="submit" name="logout" value="Logout">
                    </div>
               </div>
               </div>
          </div>
</form>
</body>
</html>
