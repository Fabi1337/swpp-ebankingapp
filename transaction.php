<?php
session_start();

require_once "models/User.php";
require_once "models/Credentials.php";
require_once "models/Transaction.php";

if(!Credentials::isLoggedIn()){
    header("Location: main.php");
    exit();
}
$message = "";
$currentUser = new User(unserialize($_SESSION['user'])->getIban());

if(isset($_POST['submit'])){
    $t = new Transaction($currentUser->getIban(), 'SWPP787', $_POST['recieverIban'], $_POST['recieverBic'], $_POST['paymentReference'], $_POST['purposeOfUse'], $_POST['amount']);
    if($t->transfer()){
        header("Location: user_main.php");
        exit();
    }
    else{
        $message = "<ul class='alert alert-danger'>";
        foreach($t->getErrors() as $error){
            $message .= "<li>" . $error . "</li>";
        }
        $message .= "</ul>";
    }

}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <title>E-Banking</title>
    <script type="text/javascript" src="js/index.js"></script>
</head>
<body style="background-image: linear-gradient(to top, rgba(0,0,0,0), rgba(0,0,0,0.3)); background-repeat: no-repeat;">
<body>
<form action="transaction.php" method="post">
        <div align="center">
            <h1>Überweisung</h1>
            <?=$message?>
            <div align="center">
                <div class="col-sm-4">
                    <input class="form-control mb-3" type="number" name="recieverIban" placeholder="Empfänger IBAN">
                    <input class="form-control mb-3" type="text" name="recieverBic" placeholder="Empfänger BIC">
                    <input class="form-control mb-3" type="text" name="paymentReference" placeholder="Zahlungsreferenz">
                    <input class="form-control mb-3" type="text" name="purposeOfUse" placeholder="Verwendungszweck">
                </div>
                </div>
                <div class="col-sm-5">
                    <div class="moneyText">Geldbetrag</div>
                    <input class="moneyInput, col-sm-6" type="number" step=".01" name="amount" placeholder="0.00 €">
                </div>
            <div class="form-group" align="center">
                <input class="btn button mb-3" type="submit" name="submit" value="Überweisen">
                <div class="input-group-btn">
                    <button class="btn btn-default button" type="submit">
                <div class="form-group">
                            <a href="user_main.php" class="btn button mb-3">Zurück</a>
                    </div>
                </div>
            </div>
        </div>
</form>
</body>
</html>