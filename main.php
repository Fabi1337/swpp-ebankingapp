<?php
session_start();

require_once "models/User.php";
require_once "models/Credentials.php";

$message = "";

if(isset($_POST['submit'])){
     $c = new Credentials();
     $c->setEmail($_POST['email']);
     $c->setPassword($_POST['password']);
     if($c->login()){
          header("Location: user_main.php");
          exit();
     }
     else{
          $message = "Fehlerhafte Anmeldedaten";
     }
}
?>

<body style="background-image: linear-gradient(to top, rgba(0,0,0,0), rgba(0,0,0,0.3)); background-repeat: no-repeat;">

<!DOCTYPE html>
<html>
<head>
    <title>E-Banking Login</title>
    <link rel="stylesheet" href="css/bootstrap.min.css" />
</head>
<body>
<br />
<div class="container" style="width:500px;">

    <h1 align="center">E-Banking Login</h1><br />
    <a style="font-weight: bold; color: red"> <?=$message?> </a>
    <form method="post">
        <label>E-Mail</label>
        <input type="text" name="email" class="form-control" />
        <br />
        <label>Passwort</label>
        <input type="password" name="password" class="form-control" />
        <br />
        <div class="mb-4"">
        <input style="background-color: black; border: whitesmoke" type="submit" name="submit" class="btn btn-primary btn-block"/>
</div>
<div class="mb-4"">
<a style=" background-color: black; border: whitesmoke" href="register.php" class="btn btn-primary btn-block">Registrieren</a>
</div>



</form>
</div>
<br />
</body>
</html>
