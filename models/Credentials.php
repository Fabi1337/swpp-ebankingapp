<?php

class Credentials
{
     private $email;
     private $password;
     private $iban;

     public static function isLoggedIn(){
          if(isset($_SESSION['user'])){
               return true;
          }
          else{
               return false;
          }
     }

     public function login(){
          if($this->loginHelper()){
               $_SESSION['user'] = serialize($this);
               return true;
          }
          else{
               return false;
          }
     }

     public static function logOut(){
          unset($_SESSION['user']);
          session_destroy();
     }

     public function loginHelper(){
          try {
               $db = Database::connect();
               $sql = "SELECT * FROM user WHERE email = ? AND password = ?";
               $stmt = $db->prepare($sql);
               $stmt->execute(array($this->email, $this->password));
               $result = $stmt->fetch(PDO::FETCH_ASSOC);
               Database::disconnect();

               if($result != null && $result['email'] == $this->email && $result['password'] == $this->password){
                    $this->iban = $result['iban'];
                    return true;
               }
               else{
                    return false;
               }
          }
          catch (PDOException $e) {
               die($e->getMessage());
          }
     }

     public function getEmail()
     {
          return $this->email;
     }

     public function setEmail($email)
     {
          $this->email = $email;
     }

     public function getPassword()
     {
          return $this->password;
     }

     public function setPassword($password)
     {
          $this->password = $password;
     }

     public function getIban()
     {
          return $this->iban;
     }

     public function setIban($iban)
     {
          $this->iban = $iban;
     }



}