<?php

class CookieHelper{

    public static function setCookie(){
        setcookie('acceptCookie', true, time() + (86400 * 30), "/");
    }

    public static function isCookieSet(){
        if(isset($_COOKIE['acceptCookie'])){
            return true;
        }
        else{
            return false;
        }
    }
}