<?php

require_once "Database.php";
require_once "Credentials.php";

class Transaction
{
     private $id = 0;
     private $senderIban;
     private $senderBic;
     private $recieverIban;
     private $recieverBic;
     private $paymentReference;
     private $purposeOfUse;
     private $date;
     private $time;
     private $amount;

     private $errors;

     public function __construct($senderIban, $senderBic, $recieverIban, $recieverBic, $paymentReference, $purposeOfUse, $amount){
          $this->senderIban = $senderIban;
          $this->senderBic = $senderBic;
          $this->recieverIban = $recieverIban;
          $this->recieverBic = $recieverBic;
          $this->paymentReference = $paymentReference;

          $this->date = date('Y-m-j');

          $s = date('d-m-Y H:i:s');
          $dt = new DateTime($s);
          $this->time = $dt->format('H:i:s');

          $this->purposeOfUse = $purposeOfUse;
          $this->amount = $amount;
     }

     public function transfer(){
          if($this->validateIban() & $this->validateBalance()){
               $db = Database::connect();
               try{
                    if($this->senderIban == unserialize($_SESSION['user'])->getIban()){
                         $sql = "INSERT INTO transactions(senderIban, senderBic, recieverIban, recieverBic, date, time, paymentReference, purposeOfUse, amount) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?) ";
                         $stmt = $db->prepare($sql);
                         $stmt->execute(array($this->senderIban, $this->senderBic, $this->recieverIban, $this->recieverBic, $this->date, $this->time, $this->paymentReference, $this->purposeOfUse, -$this->amount));
                    }
                    else{
                         $sql = "INSERT INTO transactions(senderIban, senderBic, recieverIban, recieverBic, date, time, paymentReference, purposeOfUse, amount) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?) ";
                         $stmt = $db->prepare($sql);
                         $stmt->execute(array($this->senderIban, $this->senderBic, $this->recieverIban, $this->recieverBic, $this->date, $this->time, $this->paymentReference, $this->purposeOfUse, $this->amount));
                    }

                    $sqlSender = "UPDATE user SET balance = balance - ? WHERE iban = ?";
                    $stmt = $db->prepare($sqlSender);
                    $stmt->execute(array($this->amount, $this->senderIban));
                    $sqlReciever = "UPDATE user SET balance = balance + ? WHERE iban = ?";
                    $stmt = $db->prepare($sqlReciever);
                    $stmt->execute(array($this->amount, $this->recieverIban));
                    Database::disconnect();
                    return true;
               }
               catch(PDOException $p){
                    echo $p->getMessage();
                    return false;
               }
          }
          else{
               return false;
          }
     }

     public function deposit(){
          if($this->validateIban()){
               try{
                    $db = Database::connect();
                    $sqlSender = "INSERT INTO transactions(senderIban, senderBic, recieverIban, recieverBic, date, time, paymentReference, purposeOfUse, amount) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?) ";
                    $stmt = $db->prepare($sqlSender);
                    $stmt->execute(array($this->senderIban, $this->senderBic, $this->recieverIban, $this->recieverBic, $this->date, $this->time, $this->paymentReference, $this->purposeOfUse, -$this->amount));
                    $sqlReciever = "UPDATE user SET balance = balance + ? WHERE iban = ?";
                    $stmt = $db->prepare($sqlReciever);
                    $stmt->execute(array($this->amount, $this->recieverIban));
                    Database::disconnect();
                    return true;
               }
               catch(PDOException $p){
                    echo $p->getMessage();
                    return false;
               }
          }
          else{
               return false;
          }
     }

     public function withdraw(){
          if($this->validateIban() & $this->validateWithdrawBalance()){
               try{
                    $db = Database::connect();
                    $sqlSender = "INSERT INTO transactions(senderIban, senderBic, recieverIban, recieverBic, date, time, paymentReference, purposeOfUse, amount) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?) ";
                    $stmt = $db->prepare($sqlSender);
                    $stmt->execute(array($this->senderIban, $this->senderBic, $this->recieverIban, $this->recieverBic, $this->date, $this->time, $this->paymentReference, $this->purposeOfUse, $this->amount));
                    $sqlReciever = "UPDATE user SET balance = balance - ? WHERE iban = ?";
                    $stmt = $db->prepare($sqlReciever);
                    $stmt->execute(array($this->amount, $this->recieverIban));
                    Database::disconnect();
                    return true;
               }
               catch(PDOException $p){
                    echo $p->getMessage();
                    return false;
               }
          }
          else{
               return false;
          }
     }

     public static function getJSON($dateVon, $dateBis){
          $dateVon = $dateVon != "" ? $dateVon : "0000-01-01";
          $dateBis = $dateBis != "" ? $dateBis : "9999-12-31";
          try {
               $db = Database::connect();
               $sql = "SELECT * FROM transactions WHERE senderIban = ? AND (date BETWEEN ? AND ?)";
               $stmt = $db->prepare($sql);
               $stmt->execute(array(unserialize($_SESSION['user'])->getIban(), $dateVon, $dateBis));
               $resultJSON = $stmt->fetchAll();
               Database::disconnect();
               return json_encode($resultJSON);
          } catch(PDOException $e) {
               echo $e->getMessage();
          }
     }

     public static function getJSONChart(){
          try {
               $db = Database::connect();
               $sql = "SELECT amount FROM transactions WHERE recieverIban = ? ORDER BY date ASC";
               $stmt = $db->prepare($sql);
               $stmt->execute(array(unserialize($_SESSION['user'])->getIban()));
               $resultJSON = $stmt->fetchAll();
               Database::disconnect();
               return json_encode($resultJSON);
          } catch(PDOException $e) {
               echo $e->getMessage();
          }
     }

     public function validateIban(){
          try {
               $db = Database::connect();
               $sql = "SELECT EXISTS (SELECT iban FROM user WHERE iban = ?)";
               $stmt = $db->prepare($sql);
               $stmt->execute(array($this->recieverIban));
               $result = $stmt->fetch();
               Database::disconnect();

               if($result[0] == 1 && $this->recieverIban != ""){
                    return true;
               }
               else{
                    $this->errors['iban'] = "Nicht vorhandene IBAN!";
                    return false;
               }
          }
          catch (PDOException $e) {
               die($e->getMessage());
          }
     }

     public function validateBalance(){
          try {
               $db = Database::connect();
               $sql = "SELECT balance FROM user WHERE iban = ?";
               $stmt = $db->prepare($sql);
               $stmt->execute(array($this->senderIban));
               $result = $stmt->fetch(PDO::FETCH_ASSOC);
               Database::disconnect();

               if($result['balance'] < $this->amount || $this->amount == 0){
                    $this->errors['balance'] = "Sie haben nicht genügend Guthaben!";
                    return false;
               }
               else{
                    return true;
               }
          }
          catch (PDOException $e) {
               die($e->getMessage());
          }
     }

     public function validateWithdrawBalance(){
          try {
               $db = Database::connect();
               $sql = "SELECT balance FROM user WHERE iban = ?";
               $stmt = $db->prepare($sql);
               $stmt->execute(array($this->recieverIban));
               $result = $stmt->fetch(PDO::FETCH_ASSOC);
               Database::disconnect();

               if($result['balance'] < $this->amount || $this->amount == 0){
                    $this->errors['balance'] = "Sie haben nicht genügend Guthaben!";
                    return false;
               }
               else{
                    return true;
               }
          }
          catch (PDOException $e) {
               die($e->getMessage());
          }
     }

     public function getId()
     {
          return $this->id;
     }

     public function setId($id): void
     {
          $this->id = $id;
     }

     public function getSenderIban()
     {
          return $this->senderIban;
     }

     public function setSenderIban($senderIban): void
     {
          $this->senderIban = $senderIban;
     }

     public function getSenderBic()
     {
          return $this->senderBic;
     }

     public function setSenderBic($senderBic): void
     {
          $this->senderBic = $senderBic;
     }

     public function getRecieverIban()
     {
          return $this->recieverIban;
     }

     public function setRecieverIban($recieverIban): void
     {
          $this->recieverIban = $recieverIban;
     }

     public function getRecieverBic()
     {
          return $this->recieverBic;
     }

     public function setRecieverBic($recieverBic): void
     {
          $this->recieverBic = $recieverBic;
     }

     public function getPaymentReference()
     {
          return $this->paymentReference;
     }

     public function setPaymentReference($paymentReference): void
     {
          $this->paymentReference = $paymentReference;
     }

     public function getPurposeOfUse()
     {
          return $this->purposeOfUse;
     }

     public function setPurposeOfUse($purposeOfUse): void
     {
          $this->purposeOfUse = $purposeOfUse;
     }

     public function getAmount()
     {
          return $this->amount;
     }

     public function setAmount($amount): void
     {
          $this->amount = $amount;
     }

     public function getErrors()
     {
          return $this->errors;
     }

     public function setErrors($errors): void
     {
          $this->errors = $errors;
     }

     public function getDate(): string
     {
          return $this->date;
     }

     public function setDate(string $date): void
     {
          $this->date = $date;
     }

     public function getTime()
     {
          return $this->time;
     }

     public function setTime($time): void
     {
          $this->time = $time;
     }
}