<?php

require "Database.php";

class user
{
     private $firstname = "";
     private $surname = "";
     private $email = "";
     private $password = "";
     private $role = "";
     private $iban = "";
     private $balance = "";
     private $errors = [];

     public function __construct($iban)
     {
          $this->getInfo($iban);
     }

     public function getInfo($iban){
          try {
               $db = Database::connect();
               $sql = "SELECT * FROM user WHERE iban = ?";
               $stmt = $db->prepare($sql);
               $stmt->execute(array($iban));
               $result = $stmt->fetch();
               Database::disconnect();
               if($result != null){
                    $this->firstname = $result[0];
                    $this->surname = $result[1];
                    $this->email = $result[2];
                    $this->password = $result[3];
                    $this->role = $result[4];
                    $this->iban = $result[5];
                    $this->balance = $result[6];
               }
          }
          catch (PDOException $e) {
               die($e->getMessage());
          }
     }


     public function create()
     {
          try {
               $db = Database::connect();
               $sql = "INSERT INTO user (firstname, surname, email, password, role, iban, balance) values(?, ?, ? , ? ,? ,?, ?)";
               $stmt = $db->prepare($sql);
               try {
                    $stmt->execute(array($this->firstname, $this->surname, $this->email, $this->password, "user", random_int(1111111, 99999999), 0));
               } catch (Exception $e) {
                    echo $e->getMessage();
               }
               Database::disconnect();
          } catch (PDOException $e) {
               die($e->getMessage());
          }
     }

     public function validate()
     {
          return $this->validateFirstname() & $this->validateSurname() & $this->validateEmail() & $this->validatePassword();
     }

     public function validateFirstname()
     {
          if (strlen($this->firstname) <= 20) {
               return true;
          } else {
               $this->errors['firstname'] = "Der eingegebene Vorname ist zu lang!";
               return false;
          }
     }

     public function validateSurname()
     {
          if (strlen($this->surname) <= 20) {
               return true;
          } else {
               $this->errors['surname'] = "Der eingegebene Nachname ist zu lang!";
               return false;
          }
     }

     public function validateEmail()
     {
          if (filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
               return true;
          } else {
               $this->errors['email'] = "Die eingegebene E-Mail ist nicht gültig!";
               return false;
          }
     }


     public function validatePassword()
     {
          $uppercase = preg_match('@[A-Z]@', $this->password);
          $lowercase = preg_match('@[a-z]@', $this->password);
          $number = preg_match('@[0-9]@', $this->password);
          $specialChars = preg_match('@[^\w]@', $this->password);

          if ($uppercase && $lowercase && $number && $specialChars && strlen($this->password) > 8) {
               return true;
          } else {
               $this->errors['password'] = "Das Passwort muss mindestens 8 Zeichen lang sein und muss mindestens einen Groß- und Kleinbuchstaben, eine Ziffer und ein Sonderzeichen enthalten!";
               return false;
          }
     }

     public function getFirstname()
     {
          return $this->firstname;
     }

     public function setFirstname($firstname)
     {
          $this->firstname = $firstname;
     }

     public function getSurname()
     {
          return $this->surname;
     }

     public function setSurname($surname)
     {
          $this->surname = $surname;
     }

     public function getEmail()
     {
          return $this->email;
     }

     public function setEmail($email)
     {
          $this->email = $email;
     }

     public function getPassword()
     {
          return $this->password;
     }

     public function setPassword($password)
     {
          $this->password = $password;
     }

     public function getRole()
     {
          return $this->role;
     }

     public function setRole($role)
     {
          $this->role = role;
     }


     public function getIban()
     {
          return $this->iban;
     }

     public function setIban($iban)
     {
          $this->iban = $iban;
     }

     public function getBalance()
     {
          return $this->balance;
     }

     public function setBalance($balance)
     {
          $this->balance = $balance;
     }

     public function getErrors()
     {
          return $this->errors;
     }

     public function setErrors($errors)
     {
          $this->errors = $errors;
     }

     }
