-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 07. Feb 2022 um 05:17
-- Server-Version: 10.4.22-MariaDB
-- PHP-Version: 8.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `ebanking`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `transactions`
--

CREATE TABLE `transactions` (
  `id` int(16) NOT NULL,
  `senderIban` int(128) NOT NULL,
  `senderBic` varchar(128) NOT NULL,
  `recieverIban` int(128) NOT NULL,
  `recieverBic` varchar(128) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `paymentReference` varchar(128) NOT NULL,
  `purposeOfUse` varchar(128) NOT NULL,
  `amount` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `transactions`
--

INSERT INTO `transactions` (`id`, `senderIban`, `senderBic`, `recieverIban`, `recieverBic`, `date`, `time`, `paymentReference`, `purposeOfUse`, `amount`) VALUES
(20, 5721669, 'SWPP787', 7874296, 'SWPP787', '2022-02-07', '03:31:19', 'Test', 'Test', -2),
(21, 7874296, 'SWPP787', 96711258, 'SWPP787', '2022-02-07', '05:08:27', 'Test2', 'Test2', -10.6),
(22, 7874296, 'SWPP787', 96711258, 'SWPP787', '2022-02-07', '05:09:17', 'Test3', 'Test3', -1.25);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

CREATE TABLE `user` (
  `firstname` varchar(64) NOT NULL,
  `surname` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `role` varchar(16) NOT NULL,
  `iban` int(128) NOT NULL,
  `balance` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `user`
--

INSERT INTO `user` (`firstname`, `surname`, `email`, `password`, `role`, `iban`, `balance`) VALUES
('Fabian', 'Schöpf', 'fabianschoepf@tsn.at', 'Password1!', 'user', 5721669, 11),
('Silas', 'Scheiber', 'silascheiber@tsn.at', 'Password1!', 'employee', 7874296, 32.15),
('Maximilian', 'Gstrein', 'maxwell@gmail.com', 'Password1!', 'user', 96711258, 11.85);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_recieverIban` (`recieverIban`),
  ADD KEY `fk_senderIban` (`senderIban`);

--
-- Indizes für die Tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`iban`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `fk_recieverIban` FOREIGN KEY (`recieverIban`) REFERENCES `user` (`iban`),
  ADD CONSTRAINT `fk_senderIban` FOREIGN KEY (`senderIban`) REFERENCES `user` (`iban`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
