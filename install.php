<?php
session_start();

require_once "models/Database.php";
$servername = "localhost";
$username = "root";
$password = "";

try {
    $conn = new PDO("mysql:host=$servername", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "CREATE DATABASE ebanking";
    // use exec() because no results are returned
    $conn->exec($sql);
    echo "Database created successfully<br>";
} catch(PDOException $e) {
    echo $sql . "<br>" . $e->getMessage();
}
$conn = null;
$db = Database::connect();
$sql = file_get_contents("user.sql");
$stmt = $db->prepare($sql);
$stmt->execute();
Database::disconnect();
header('Location: index.php');
?>