<?php
session_start();

require_once "models/User.php";
require_once "models/Credentials.php";
require_once "models/Transaction.php";

if(!Credentials::isLoggedIn()){
    header("Location: main  .php");
    exit();
}
$currentUser = new User(unserialize($_SESSION['user'])->getIban());

if(isset($_POST['logout'])){
    Credentials::logOut();
    header("Location: main.php");
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/stylesheet.css" rel="stylesheet">

    <title>E-Banking</title>


    <script>
        function loadText(){
            var xhr = new XMLHttpRequest();
            xhr.open('GET', 'transactionDisplayList.php?dateVon=' + document.getElementById('dateVon').value +
                '&dateBis=' + document.getElementById('dateBis').value);

            xhr.onload = function(){
                if(this.status == 200){
                    var userData = JSON.parse(this.responseText)
                    var htmlString = "<tr>" +
                        "<th>Id</th>" +
                        "<th>Sender</th>" +
                        "<th>SBIC</th>" +
                        "<th>Empfänger</th>" +
                        "<th>EBIC</th>" +
                        "<th>Datum</th>" +
                        "<th>Uhrzeit</th>" +
                        "<th>Zahlungsreferenz</th>" +
                        "<th>Verwendungszweck</th>" +
                        "<th>Betrag</th>" +
                        "</tr>";
                    for(var userDataKey of userData) {
                        var num = new Number(userDataKey.amount).toFixed(2);
                        htmlString += "<tr> " +
                            "<td>"+ userDataKey.id +"</td>" +
                            "<td>"+ userDataKey.senderIban +"</td>" +
                            "<td>"+ userDataKey.senderBic +"</td>" +
                            "<td>"+ userDataKey.recieverIban +"</td>" +
                            "<td>"+ userDataKey.recieverBic +"</td>" +
                            "<td>"+ userDataKey.date +"</td>" +
                            "<td>"+ userDataKey.time +"</td>" +
                            "<td>"+ userDataKey.paymentReference +"</td>" +
                            "<td>"+ userDataKey.purposeOfUse +"</td>" +
                            "<td>"+ num +" €</td>" +
                            "</tr>";
                    }
                    var table = document.getElementById("table");
                    table.innerHTML = htmlString;
                }
            }
            xhr.send();
        }
    </script>

<body onload="loadText()" style="background-image: linear-gradient(to top, rgba(0,0,0,0), rgba(0,0,0,0.3)); background-repeat: no-repeat;">
<form action="user_main.php" method="post">
    <div class="card center shadow-lg" style="width: 60%; margin-top: 50px">
        <div class="row">
            <div class="card-body">
                <div class="form-inline">
                    <div class="form-group ">
                        <label>Datum</label>
                        <input class="form-control m-1" type="text" onchange="loadText()" id="dateVon" placeholder="Von">
                        <input class="form-control m-1" type="text" onchange="loadText()" id="dateBis" placeholder="Bis">
                    </div>
                </div>
                <table class="table table-responsive" id="table"></table>
                <a href="user_main.php" class="btn button mb-3">Zurück</a>
            </div>
        </div>
    </div>
</form>
</body>
</html>