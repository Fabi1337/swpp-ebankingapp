<?php
session_start();

require_once "models/User.php";
require_once "models/Credentials.php";
require_once "models/Transaction.php";

if(!Credentials::isLoggedIn()){
    header("Location: login.php");
    exit();
}
$currentUser = new User(unserialize($_SESSION['user'])->getIban());

if(isset($_POST['logout'])){
    Credentials::logOut();
    header("Location: login.php");
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/stylesheet.css" rel="stylesheet">

    <title>e-Banking Nino</title>

</head>
<body onload="loadText()">
<form action="userIndex.php" method="post">
    <div class="card center shadow-lg" style="width: 85%; margin-top: 50px">
        <div class="row">
            <div class="col-sm-5">
                <div class="card-body">
                    <h1 class="card-title"><?=$currentUser->getFirstname() . " " . $currentUser->getSurname()?></h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <div class="card shadow-lg utilityBox">
                    <div class="card-body float-left">
                        <a href="transactionEmployee.php" class="card-title">Kassa</a>
                        <img src="img/icon/arrow-bar-right.svg" class="float-right mt-1">
                    </div>
                </div>
            </div>
        </div>
        <hr/>

        <div class="input-group">
            <input class="btn logoutButton" type="submit" name="logout" value="Logout">
            <div class="input-group-btn">
                <button class="btn btn-default logoutButton" type="submit">
                    <img src="img/icon/box-arrow-right.svg">
                </button>
            </div>
        </div>
    </div>
</form>
</body>
</html>