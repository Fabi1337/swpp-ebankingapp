<?php
session_start();

require_once "models/User.php";
require_once "models/Credentials.php";

if(!Credentials::isLoggedIn()){
     header("Location: main.php");
     exit();
}
$currentUser = new User(unserialize($_SESSION['user'])->getIban());

if($currentUser->getRole() === "user"){
     header("Location: user_main.php");
     exit();
}
else{
     header("Location: employeeIndex.php");
     exit();
}