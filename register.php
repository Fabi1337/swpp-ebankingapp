<?php
require "models/User.php";
$message = "";

if(isset($_POST['submit'])){
     $u = new User(0);
     $u->setFirstname($_POST['firstname']);
     $u->setSurname($_POST['surname']);
     $u->setEmail($_POST['email']);
     $u->setPassword($_POST['password']);
     if($u->validate()){
          $u->create();
          header("Location: index.php");
     }
     else{
          $message = "<ul class='alert-danger border border-danger rounded'>";
          foreach($u->getErrors() as $error){
               $message .= "<li>" . $error . "</li>";
          }
          $message .= "</ul>";
     }
}
?>


<!doctype html>
<html lang="en">
<head>
     <!-- Required meta tags -->
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

     <!-- Bootstrap CSS -->
     <link rel="stylesheet" href="css/bootstrap.min.css">

     <title>Registrierung</title>

     <script type="text/javascript" src="js/index.js"></script>

</head>
<body style="background-image: linear-gradient(to top, rgba(0,0,0,0), rgba(0,0,0,0.3)); background-repeat: no-repeat;">

<div class="container">
     <form id="form_grade" action="register.php" method="post">
          <div style="margin-left: 30%; margin-right: 30%;">
               <h1 class="mt-5 mb-3">Registrierung</h1>
               <div class="form-group required">
                    <label class="control-label">Vorname *</label>
                    <input type="text" class="form-control" name="firstname">
                         <div class="help-block"></div>
               </div>

                    <div class="form-group required">
                         <label class="control-label">Nachname *</label>
                         <input type="text" class="form-control" name="surname">
               </div>
                    <div class="form-group required">
                         <label class="control-label">Email *</label>
                         <input type="email" class="form-control" name="email">
               </div>

                    <div class="form-group required">
                         <label class="control-label">Passwort *</label>
                         <input type="password" class="form-control" name="password">
               </div>
               <div class="mb-4">
                    <input type="submit"
                           name="submit"
                           class="btn btn-primary btn-block"
                           style="background-color: black; border: none"
                           value="Registrieren">
               </div>


               <div class="mb-4"">
               <a style="background-color: black; border: whitesmoke" href="index.php" class="btn btn-primary btn-block">Zurück zur Anmeldung</a>
          </div>


          <div>
               <a style="background-color: gray; border: whitesmoke" href="register.php" class="btn btn-secondary btn-block">Löschen</a>
          </div>

</div>
</form>
</div>
</body>
</html>
