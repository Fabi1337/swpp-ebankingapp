-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 23, 2022 at 01:19 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ebanking`
--
--
-- Table structure for table `user`
--

CREATE TABLE `user`
(
    `firstname` varchar(64) NOT NULL,
    `surname`   varchar(64) NOT NULL,
    `email`     varchar(64) NOT NULL,
    `password`  varchar(64) NOT NULL,
    `role`      varchar(16) NOT NULL,
    `iban`      int(128) NOT NULL,
    `balance`   double NOT NULL

) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

--
-- Table structure for table `transactions'
--


CREATE TABLE `transactions` (
    `id` int(16) NOT NULL,
    `senderIban` int(128) NOT NULL,
    `senderBic` varchar(128) NOT NULL,
    `recieverIban` int(128) NOT NULL,
    `recieverBic` varchar(128) NOT NULL,
    `date` date NOT NULL,
    `time` time NOT NULL,
    `paymentReference` varchar(128) NOT NULL,
    `purposeOfUse` varchar(128) NOT NULL,
    `amount` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


--
-- Dumping data for table `user`
--

INSERT INTO `user` (`firstname`, `surname`, `email`, `password`, `role`, `iban`, `balance`)
VALUES ('Silas', 'Scheiber', 'silascheiber@tsn.at', 'Password1!', 'employee', 7874296, 42),
       ('Fabian', 'Schöpf', 'fabianschoepf@tsn.at', 'Password1!', 'user', 5721669, 13);


-- Indizes für die Tabelle `transactions`
--
ALTER TABLE `transactions`
    ADD PRIMARY KEY (`id`),
    ADD KEY `fk_recieverIban` (`recieverIban`),
    ADD KEY `fk_senderIban` (`senderIban`);

--
-- Indizes für die Tabelle `users`
--
ALTER TABLE `user`
    ADD PRIMARY KEY (`iban`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `transactions`
--
ALTER TABLE `transactions`
    MODIFY `id` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `transactions`
--
ALTER TABLE `transactions`
    ADD CONSTRAINT `fk_recieverIban` FOREIGN KEY (`recieverIban`) REFERENCES `user` (`iban`),
    ADD CONSTRAINT `fk_senderIban` FOREIGN KEY (`senderIban`) REFERENCES `user` (`iban`);
COMMIT;

-- --------------------------------------------------------

/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
